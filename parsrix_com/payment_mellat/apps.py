from django.apps import AppConfig


class PaymentMellatConfig(AppConfig):
    name = 'payment_mellat'
