# -*- coding: utf-8 -*-
from django.http import HttpResponse
# from django.shortcuts import redirect
from zeep import Client
import requests
from datetime import datetime
from django.views.decorators.http import require_POST
import random


def initialize_bp_pay_request_fields():
    now = datetime.now()
    payment = {
        'client': Client('http://banktest.ir/gateway/mellat/ws?wsdl'),
        'terminalId': 274,
        'userName': "user274",
        'userPassword': "26005836",
        'orderId': random.randint(21, 1000),
        'amount': 120000,
        'localDate': str(now.strftime("%Y%m%d")),
        'localTime': str(now.strftime("%H%M%S")),
        # TODO: consider format policy for additionalData
        'additionalData': "this is a test",
        'callBackUrl': 'http://localhost:8000/mellat/verify/',
        'payerId': random.randint(21, 1000),
        # subServiceId =  5588,
    }
    return payment


def initialize_bp_verify_request_fields():
    # TODO: continue here
    now = datetime.now()
    payment = {
        'client': Client('http://banktest.ir/gateway/mellat/ws?wsdl'),
        'terminalId': 274,
        'userName': "user274",
        'userPassword': "26005836",
        'orderId': random.randint(21, 1000),
        'amount': 120000,
        'localDate': str(now.strftime("%Y%m%d")),
        'localTime': str(now.strftime("%H%M%S")),
        # TODO: consider format policy for additionalData
        'additionalData': "this is a test",
        'callBackUrl': 'http://localhost:8000/mellat/verify/',
        'payerId': random.randint(21, 1000),
        # subServiceId =  5588,
    }
    return payment


def send_request(request):
    # TODO: get needed fields for payment from post request
    # TODO: add require_POST decorator for this function
    # TODO: consider csrf protect for this function for post request
    # TODO: consider session timeout greater than 15 minutes if needed to session
    payment_data = initialize_bp_pay_request_fields()
    # check bpPayRequest and get response code and ref id
    result = payment_data['client'].service.bpPayRequest(payment_data['terminalId'], payment_data['userName'],
                                                         payment_data['userPassword'], payment_data['orderId'],
                                                         payment_data['amount'], payment_data['localDate'],
                                                         payment_data['localTime'], payment_data['additionalData'],
                                                         payment_data['callBackUrl'], payment_data['payerId'])
    response_code, ref_id,  = result.split(',')
    print("result: code={}, hash={}".format(response_code, ref_id))
    if int(response_code) == 0:
        response = requests.post('http://banktest.ir/gateway/mellat/gate', data=payment_data)
        content = response.content
        return HttpResponse(content)
    else:
        # TODO: consider try again for new ref id
        return HttpResponse('پرداخت با اشکال مواجه شد. کد خطا: ' + str(response_code))


@require_POST
def verify(request):
    # TODO: consider security in POST request
    # TODO: process POST variables inside website
    ref_id = request.POST['RefId']
    response_code = request.POST['ResCode']
    sale_order_id = request.POST['SaleOrderId']
    sale_reference_id = request.POST['SaleReferenceId']
    # TODO: consider and check session timeout not expired
    payment_data = initialize_bp_verify_request_fields()
    result = payment_data['client'].service.bpPayRequest(payment_data['terminalId'], payment_data['userName'],
                                                         payment_data['userPassword'], payment_data['orderId'],
                                                         payment_data['amount'], payment_data['localDate'],
                                                         payment_data['localTime'], payment_data['additionalData'],
                                                         payment_data['callBackUrl'], payment_data['payerId'])
    return HttpResponse('post data: {}'.format(request.POST))
