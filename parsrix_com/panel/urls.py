from django.urls import path, include
from . import views

urlpatterns = [
    # panel:
    path('', views.select_panel, name="select-panel"),
    path('login/', views.login_register, name="user-authentication"),
    path('logout/', views.logout, name="user-logout"),
    # backer panel:
    path('backer/', include('panel.backer.urls')),
    # thinker panel:
    path('thinker/', include('panel.thinker.urls')),
]
