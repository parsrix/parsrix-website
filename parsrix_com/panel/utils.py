from django.contrib.auth.models import Group
from django.http import JsonResponse
from json import JSONEncoder


def json_response(response_data):
    """convert dictionary context to json"""
    return JsonResponse(response_data, safe=False, encoder=JSONEncoder)


def is_authenticated_in_group(u, g):
    # TODO: apply correct security policy for users such as is_backer, is_thinker,... field and group_required decorator
    if u.is_active and u.is_authenticated:
        group = Group.objects.get(name=g)
        if group in u.groups.all() or u.is_superuser:
            return True
    return False


def json_response_special(status, message, errors=None):
    """generate context and convert to json"""
    if errors is None:
        errors = {}
    response_data = {
        'status': status,
        'message': message,
        'errors': errors,
    }
    return JsonResponse(response_data, safe=False, encoder=JSONEncoder)
