from django import forms
from captcha.fields import CaptchaField
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from .models import ClientProfile
from parsrix_com.utils import captcha_challenge
from django.core.exceptions import ValidationError


# TODO: add correct validation to database fields
# TODO: consider remember me function
class LoginForm(forms.Form):
    # username is integer because use nationality_code as username
    username_login = forms.IntegerField(widget=forms.TextInput(attrs={'type': 'tel'}))
    password_login = forms.CharField(max_length=255,
                                     widget=forms.PasswordInput())
    login_captcha = CaptchaField(id_prefix='login')
    remember_me = forms.BooleanField(initial=False, required=False)


class RegisterFormPart1(forms.ModelForm):
    password_reg = forms.CharField(label="Password", widget=forms.PasswordInput)
    re_password_reg = forms.CharField(label="Repeat Password", widget=forms.PasswordInput)
    confirm_form = forms.BooleanField(initial=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # username field settings for template
        self.fields['username'].widget = forms.TextInput()
        self.fields['username'].widget.attrs.update(type='tel')
        # show help text for password field
        self.fields['password_reg'].help_text = password_validation.password_validators_help_texts()

    def is_valid(self):
        valid = super(RegisterFormPart1, self).is_valid()
        if not valid:
            if (type(int(self.fields['username'])) is not int) or len(self.fields['username']) != 10:
                self.add_error('username', 'کدملی وارد شده معتبر نیست')
            return False
        try:
            # check username is nationality code or
            if type(int(self.cleaned_data['username'])) is int:
                try:
                    if not self.cleaned_data['confirm_form']:
                        self.add_error('confirm_form', 'قوانین را تایید کنید')
                        return False
                    is_unique = True
                    if User.objects.filter(username=self.cleaned_data['username']).exists():
                        is_unique = False
                        self.add_error('national_code', 'این کدملی قبلا ثبت شده است')
                    if User.objects.filter(email=self.cleaned_data['email']).exists():
                        is_unique = False
                        self.add_error('email', 'این ایمیل قبلا ثبت شده است')
                    if not is_unique:
                        return False
                    if self.cleaned_data['password_reg'] != self.cleaned_data['re_password_reg']:
                        self.add_error('re_password_reg', 'رمزعبور و تکرار رمزعبور یکسان نیست')
                        return False
                    try:
                        # validate password strength
                        password_validation.validate_password(self.cleaned_data['password_reg'],
                                                              self.cleaned_data['username'])
                    except ValidationError as e:
                        # TODO: add exception into logger
                        self.add_error('password_reg', e)
                        return False
                    except Exception as e:
                        # TODO: add exception into logger
                        self.add_error('password_reg', e)
                        print(type(e))  # the exception instance
                        print(e.args)  # arguments stored in .args
                        print(e)
                        return False
                except Exception as e:
                    # TODO: add exception into logger
                    print(type(e))  # the exception instance
                    print(e.args)  # arguments stored in .args
                    print(e)
                    return False
        except ValueError:
            # TODO: add exception into logger
            self.add_error('username', 'کدملی وارد شده معتبر نیست')
            return False
        except Exception as e:
            # TODO: add exception into logger
            print(type(e))  # the exception instance
            print(e.args)  # arguments stored in .args
            print(e)
            return False
        return True


class RegisterFormPart2(forms.ModelForm):
    register_captcha = CaptchaField(id_prefix='register', generator=captcha_challenge)

    class Meta:
        model = ClientProfile
        fields = ('phone_number', 'gender')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # gender field settings
        self.fields['gender'].widget = forms.RadioSelect()
        self.fields['gender'].label = 'جنسیت'
        self.fields['gender'].choices = ((0, 'زن'), (1, 'مرد'))
        # phone number settings
        self.fields['phone_number'].widget = forms.TextInput()
        self.fields['phone_number'].widget.attrs.update(type='tel')
