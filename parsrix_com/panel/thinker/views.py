from django.shortcuts import render
# decorators
from django.contrib.auth.decorators import login_required


@login_required
def panel(request):
    return render(request, 'thinker/panel_thinker.html')


@login_required
def guide(request):
    return render(request, 'thinker/guide_thinker.html')


@login_required
def introduction(request):
    return render(request, 'thinker/introduction_thinker.html')


@login_required
def reporting(request):
    return render(request, 'thinker/reporting_thinker.html')


@login_required
def complaint(request):
    return render(request, 'thinker/complaint_thinker.html')


@login_required
def submit_idea(request):
    return render(request, 'thinker/submit_idea_thinker.html')


@login_required
def track_idea(request):
    return render(request, 'thinker/track_idea_thinker.html')


@login_required
def edit_info(request):
    return render(request, 'thinker/edit_info_thinker.html')
