from django.urls import path
from . import views

urlpatterns = [
    path('', views.panel, name="thinker-panel"),
    path('guide/', views.guide, name="thinker-guide"),
    path('introduction/', views.introduction, name="thinker-introduction"),
    path('reporting/', views.reporting, name="thinker-reporting"),
    path('complaint/', views.complaint, name="thinker-complaint"),
    path('submit_idea/', views.submit_idea, name="thinker-submit_idea"),
    path('track_idea/', views.track_idea, name="thinker-track_idea"),
    path('edit-info/', views.edit_info, name="edit-info"),
]
