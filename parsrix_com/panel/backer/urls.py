from django.urls import path
from . import views

urlpatterns = [
    path('', views.panel, name="backer-panel"),
    path('reports/', views.reports, name="backer-reports"),
    path('polls/', views.polls, name="backer-polls"),
    path('charity/', views.charity, name="backer-charity"),
    path('inbox/', views.inbox, name="backer-inbox"),
    path('complaint/', views.complaint, name="backer-complaint"),
    # path('edit-info/', views.edit_info, name="edit-info"),
]
