from django.shortcuts import render
# decorators
from django.contrib.auth.decorators import login_required


@login_required
def panel(request):
    return render(request, 'backer/panel_backer.html')


@login_required
def reports(request):
    return render(request, 'backer/reports_backer.html')


@login_required
def polls(request):
    return render(request, 'backer/polls_backer.html')


@login_required
def charity(request):
    return render(request, 'backer/charity_backer.html')


@login_required
def inbox(request):
    return render(request, 'backer/inbox_backer.html')


@login_required
def complaint(request):
    return render(request, 'backer/complaint_backer.html')

