from django.db import models
from django.contrib.auth.models import User


# TODO: data validation check for database
# TODO: fix default value to null for non require fields
class ClientProfile(models.Model):
    GENDER_CHOICES = (
        (0, 'زن'),
        (1, 'مرد')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=False, primary_key=True)
    phone_number = models.IntegerField(blank=False, null=False)
    # gender = models.CharField(max_length=3, choices=GENDER_CHOICES, default='مرد')
    gender = models.IntegerField(choices=GENDER_CHOICES, default=1)
    nationality_id = models.IntegerField(null=True)
    birthday = models.DateField(null=True)
    post_code = models.IntegerField(null=True)
    telephone_number = models.IntegerField(null=True)
    address = models.TextField(blank=True)
    authentication_picture = models.CharField(max_length=255, blank=True)
    # helpful for check if user is authenticated
    authentication_mode = models.PositiveIntegerField(default=0)
    # instagram_id = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return "{}".format(self.user)
