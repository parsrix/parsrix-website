from django.shortcuts import render, redirect
# authentication and authorization
from django.contrib import auth
from django.contrib.auth.models import User
# decorators
from django.contrib.auth.decorators import login_required
# app packages
from .models import ClientProfile
from .forms import LoginForm, RegisterFormPart1, RegisterFormPart2
# other packages
from .utils import *


def login_register(request):
    # if is_authenticated_in_group(request.user, 'client'):
    if request.user.is_active and request.user.is_authenticated:
        return redirect('/panel/')
    else:
        # TODO: add exceptions
        if request.method == "POST":
            # --------------- login form ---------------#
            if request.POST.get('username_login', '') and request.POST.get('password_login', ''):
                my_register_form_part1 = RegisterFormPart1()
                my_register_form_part2 = RegisterFormPart2()
                # TODO: validate form fields
                # TODO: clean form fields
                my_login_form = LoginForm(request.POST)
                if my_login_form.is_valid():
                    username = str(my_login_form.cleaned_data['username_login'])
                    password = my_login_form.cleaned_data['password_login']
                    try:
                        client_user = auth.authenticate(username=username, password=password)
                    except Exception as e:
                        # TODO: add exception into logger
                        print(type(e))  # the exception instance
                        print(e.args)  # arguments stored in .args
                        print(e)
                        my_login_form.add_error(
                            None, 'مشکلی در ورود به سایت وجود دارد. با پشتیبانی تماس بگیرید')
                        return render(request, 'panel/register-login.html',
                                      {'my_login_form': my_login_form, 'my_register_form_part1': my_register_form_part1,
                                       'my_register_form_part2': my_register_form_part2})
                    # TODO: check user group as client
                    # TODO: check is_active user for users is not active or banned
                    if client_user is not None and client_user.is_active and client_user.is_authenticated:
                        try:
                            auth.login(request, client_user)
                            return redirect('/panel/')
                        except Exception as e:
                            # TODO: add exception into logger
                            print(type(e))  # the exception instance
                            print(e.args)  # arguments stored in .args
                            print(e)
                            my_login_form.add_error(
                                None, 'مشکلی در ورود به سایت وجود دارد. با پشتیبانی تماس بگیرید')
                    # if user can not authenticate
                    else:
                        my_login_form.add_error(None, 'کد ملی یا رمزعبور صحیح نیست')
                        my_login_form.add_error('password_login', 'کد ملی یا رمزعبور صحیح نیست')
                # if login form is not valid
                else:
                    my_login_form.add_error(None, 'کد ملی یا رمزعبور صحیح نیست')
                    my_login_form.add_error('password_login', 'کد ملی یا رمزعبور صحیح نیست')
                context = {
                    'my_register_form_part1': my_register_form_part1,
                    'my_register_form_part2': my_register_form_part2,
                    'my_login_form': my_login_form,
                }
                return render(request, 'panel/register-login.html', context)
            # --------------- login form ---------------#
            # --------------- register form ---------------#
            else:
                my_register_form_part1 = RegisterFormPart1(request.POST)
                my_register_form_part2 = RegisterFormPart2(request.POST)
                # TODO: add new group as client programmatically
                # TODO: consider password strength
                # TODO: check changing register captcha on ajax request
                if my_register_form_part1.is_valid() and my_register_form_part2.is_valid():
                    try:
                        user_group = Group.objects.get(name='client')
                        # create a new user in users table
                        new_client = User.objects.create_user(username=my_register_form_part1.cleaned_data['username'],
                                                              password=my_register_form_part1.cleaned_data[
                                                                  'password_reg'],
                                                              email=my_register_form_part1.cleaned_data['email'],
                                                              first_name=my_register_form_part1.cleaned_data[
                                                                  'first_name'],
                                                              last_name=my_register_form_part1.cleaned_data[
                                                                  'last_name'])
                        # create a new client
                        new_client.groups.add(user_group)
                        new_client.save()
                        new_profile = ClientProfile(user=new_client,
                                                    gender=my_register_form_part2.cleaned_data['gender'],
                                                    phone_number=my_register_form_part2.cleaned_data['phone_number'])
                        new_profile.save()
                        return json_response_special('ok',
                                                     'ثبت نام با موفقیت انجام شد، می توانید به سایت وارد شوید')
                    except Exception as e:
                        # TODO: add exception into logger
                        print(type(e))  # the exception instance
                        print(e.args)  # arguments stored in .args
                        print(e)
                        return json_response_special('failed',
                                                     'ثبت نام با مشکل مواجه شد، با پشتیبانی تماس بگیرید',
                                                     my_register_form_part1.errors)
                # if register form is not valid
                else:
                    return json_response_special('failed', 'ثبت نام با مشکل مواجه شد، خطاهای موجود را رفع نمایید',
                                                 my_register_form_part1.errors)
        else:
            my_login_form = LoginForm()
            my_register_form_part1 = RegisterFormPart1()
            my_register_form_part2 = RegisterFormPart2()
            context = {
                'my_register_form_part1': my_register_form_part1,
                'my_register_form_part2': my_register_form_part2,
                'my_login_form': my_login_form,
            }
            return render(request, 'panel/register-login.html', context)


def select_panel(request):
    if request.user.is_authenticated:
        return render(request, 'panel/select-panel.html')
    else:
        return redirect('/panel/login/')


@login_required
def logout(request):
    auth.logout(request)
    # TODO: redirect to index must be standardize
    return redirect('/')
