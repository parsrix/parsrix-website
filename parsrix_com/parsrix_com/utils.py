import random


def captcha_challenge():
    """captcha challenge function"""
    ret = u''
    for i in range(6):
        digit = random.randint(0, 9)
        if digit == 0:
            digit = digit + 1
        ret += str(digit)
    return ret, ret
