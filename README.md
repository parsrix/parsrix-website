# PARSRIX Modern CrowdFunding System
=========================================
## Security
-----------------------------------------

#### Authentication and Authorization:
-----------------------------------------
1. [ ] programmatically add __client__ group
2. [ ] define permissions for __client__ group
3. [ ] consider permission check in all views
4. [x] consider data validation for database
5. [ ] consider password strength for register forms
6. [x] consider captcha in register and login forms


## Client
-----------------------------------------
1. Register/Login Page:
    1. [x] base login system using form
    2. [x] base register system using ajax
    3. [x] error and message handling in register_login template
2. Panel Page:
    1. [x] logout function
3.

2. Panel Page:
    1. [x] logout function
3.